import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class Skyjo {

    private final Stack<Card> drawStack;
    private final Stack<Card> discardPile;
    private final List<Player> players;
    private int closingIndex = -1;
    private int currentIndex;
    private Scanner scanner = new Scanner(System.in);

    enum DrawOption {
        DECK,
        DISCARD_PILE;
    }

    public Skyjo(int numPlayers) {
        if (numPlayers < 2 || numPlayers > 8) {
            throw new IllegalArgumentException("Illegal number of players (2-8 allowed).");
        }

        players = new ArrayList<>();
        drawStack = getShuffledStack(getAllCards());
        discardPile = new Stack<>();

        for (int i = 0; i < numPlayers; i++) {
            Stack<Card> playerHand = new Stack<>();
            for (int c = 0; c < 12; c++) {
                playerHand.push(drawStack.pop());
            }
            players.add(new Player(playerHand));
        }

        Card firstCard = drawStack.pop();
        firstCard.reveal();
        discardPile.push(firstCard);

        for (Player player : players) {
            player.initialReveal();
        }

        int beginner = 0;
        int maxScore = players.get(0).getCurrentScore();
        for (int i = 1; i < players.size(); i++) {
            int score = players.get(i).getCurrentScore();
            if (score > maxScore) {
                beginner = i;
            }
        }
        currentIndex = beginner;
    }

    private static Stack<Card> getAllCards() {
        Stack<Card> cards = new Stack<>();
        addCardToStack(cards, -2, 5);
        addCardToStack(cards, -1, 10);
        addCardToStack(cards, 0, 15);
        for (int i = 1; i <= 12; i++) {
            addCardToStack(cards, i, 10);
        }
        return cards;
    }

    private static Stack<Card> getShuffledStack(Stack<Card> cards) {
        List<Card> cardList = new ArrayList<>(cards);
        Collections.shuffle(cardList);
        Stack<Card> shuffledStack = new Stack<>();
        shuffledStack.addAll(cardList);
        return shuffledStack;
    }

    private static void addCardToStack(Stack<Card> stack, int value, int count) {
        for (int i = 0; i < count; i++) {
            stack.push(new Card(value));
        }
    }

    public void print() {
        for (int i = 0; i < players.size(); i++) {
            Player player = players.get(i);
            System.out.println("Player " + (i + 1));
            player.printDeck();
            System.out.println("Score: " + player.getCurrentScore());
        }
        System.out.println("Discard Pile: " + discardPile.peek().toString());
        System.out.println("Player " + (currentIndex + 1) + "'s turn.");
        players.get(currentIndex).printDeck();
    }

    public void turn() {
        Player current = players.get(currentIndex);
        DrawOption option = drawOption();
        Card drawnCard;
        if (option == DrawOption.DECK) {
            drawnCard = drawStack.pop();
            drawnCard.reveal();
            System.out.println(drawnCard);
            discardOrKeep(drawnCard, current);
        } else {
            drawnCard = discardPile.pop();
            replace(drawnCard, current);
        }
        current.cleanHand();

        if (closingIndex == -1 && current.isAllCardsRevealed()) {
            System.out.println("Last Round!");
            closingIndex = currentIndex;
        }

        currentIndex = (currentIndex + 1) % players.size();
    }

    public void discardOrKeep(Card currentCard, Player player) {
        System.out.println("(D)iscard or Keep (X,Y)?");
        String choice = scanner.nextLine().replace(" ", "");;
        if (choice.equals("D")) {
            discardPile.push(currentCard);
            revealCard(player);
        } else if (choice.equals("K")) {
            replace(currentCard, player);
        } else {
            discardOrKeep(currentCard, player);
        }
    }

    private void revealCard(Player player) {
        System.out.println("Reveal which card? (X,Y)");
        String choice = scanner.nextLine().replace(" ", "");
        String[] coords = choice.split(",");
        int x = Integer.parseInt(coords[0]);
        int y = Integer.parseInt(coords[1]);
        if (x < 1 || x > player.getRowCount()) {
            System.out.println("Invalid X coordinate (1-" + player.getRowCount() + " allowed).");
            revealCard(player);
        }
        if (y < 1 || y > 3) {
            System.out.println("Invalid Y coordinate (1-3 allowed).");
            revealCard(player);
        }
        if (!player.reveal(x-1, y-1)) {
            System.out.println("Card is already revealed.");
            revealCard(player);
        }
    }

    public void replace(Card currentCard, Player player) {
        System.out.println("Replace with? (X,Y)");
        String choice = scanner.nextLine().replace(" ", "");
        String[] coords = choice.split(",");
        int x = Integer.parseInt(coords[0]);
        int y = Integer.parseInt(coords[1]);
        if (x < 1 || x > player.getRowCount()) {
            System.out.println("Invalid X coordinate (1-" + player.getRowCount() + " allowed).");
            replace(currentCard, player);
        }
        if (y < 1 || y > 3) {
            System.out.println("Invalid Y coordinate (1-3 allowed).");
            replace(currentCard, player);
        }

        Card swappedCard = player.swapCard(x-1, y-1, currentCard);
        swappedCard.reveal();
        discardPile.push(swappedCard);
    }

    public DrawOption drawOption() {
        do {
            System.out.println("(D)eck or Discard (P)ile: " + peekDiscardPile().toString() + "?");
            String choice = scanner.nextLine();
            if (choice.equals("D")) {
                return DrawOption.DECK;
            } else if (choice.equals("P")) {
                return DrawOption.DISCARD_PILE;
            }
        } while (true);
    }

    public static void main(String[] args) {
        Skyjo skyjo = new Skyjo(2);
        while (skyjo.currentIndex != skyjo.closingIndex) {
            skyjo.print();
            skyjo.turn();
        }

        List<Player> playerList = skyjo.players;
        for (int i = 0; i < playerList.size(); i++) {
            Player player = playerList.get(i);
            player.revealAll();
            System.out.println("Player " + (i+1) + ": " + player.getCurrentScore() + " Points");
        }
    }

    public Card peekDiscardPile() {
        return discardPile.peek();
    }


}
