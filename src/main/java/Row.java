import java.util.Iterator;
import java.util.List;

public class Row implements Iterable<Card> {

    private List<Card> cards;

    public Row(List<Card> cards) {
        if (cards.size() != 3) {
            throw new IllegalArgumentException("Row MUST have 3 cards");
        }
        this.cards = cards;
    }

    public Card get(int index) {
        return cards.get(index);
    }

    public boolean canBeCleared() {
        int val = cards.get(0).getValue();
        for (Card card : cards) {
            if (!card.isRevealed()) {
                return false;
            }
            if (val != card.getValue()) {
                return false;
            }
        }
        return true;
    }

    public Card replace(int i, Card replacement) {
        Card old = cards.remove(i);
        cards.add(i, replacement);
        return old;
    }

    @Override
    public Iterator<Card> iterator() {
        return cards.iterator();
    }
}
