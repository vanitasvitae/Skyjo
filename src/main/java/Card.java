public class Card {

    private boolean revealed = false;
    private final int value;

    public Card(int value) {
        this.value = value;
    }

    public boolean isRevealed() {
        return revealed;
    }

    public void reveal() {
        this.revealed = true;
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        return revealed ? Integer.toString(value) : "?";
    }
}
