import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

public class Player {

    private List<Row> hand = new ArrayList<>();
    private Random random = new Random();

    public Player(Stack<Card> hand) {
        if (hand.size() != 12) {
            throw new IllegalArgumentException("Invalid number of cards dealt.");
        }
        for (int i = 0; i < 4; i++) {
            List<Card> rowCards = new ArrayList<>();
            for (int j = 0; j < 3; j++) {
                rowCards.add(hand.pop());
            }
            Row row = new Row(rowCards);
            this.hand.add(row);
        }
    }

    public void printDeck() {
        for (int cardIndex = 0; cardIndex < 3; cardIndex++) {
            StringBuilder line = new StringBuilder("[");
            for (int rowIndex = 0; rowIndex < hand.size(); rowIndex++) {
                Row row = hand.get(rowIndex);
                line.append(row.get(cardIndex).toString());
                if (rowIndex != hand.size() - 1) {
                    line.append(",");
                }
            }
            line.append("]");
            System.out.println(line);
        }
    }

    public void initialReveal() {
        for (int i = 0; i < 2; i++) {
            int rowIndex = random.nextInt(hand.size());
            Row row = hand.get(rowIndex);
            int cardIndex = random.nextInt(3);
            while (row.get(cardIndex).isRevealed()) {
                cardIndex = random.nextInt(3);
            }
            row.get(cardIndex).reveal();
        }
    }

    public int getCurrentScore() {
        int sum = 0;
        for (Row row : hand) {
            for (Card card : row) {
                sum += card.isRevealed() ? card.getValue() : 0;
            }
        }
        return sum;
    }

    public boolean isAllCardsRevealed() {
        for (Row row : hand) {
            for (Card card : row) {
                if (!card.isRevealed()) {
                    return false;
                }
            }
        }
        return true;
    }

    public Card swapCard(int x, int y, Card currentCard) {
        Row row = hand.get(x);
        return row.replace(y, currentCard);
    }

    public boolean reveal(int x, int y) {
        Card card = hand.get(x).get(y);
        if (card.isRevealed()) {
            return false;
        } else {
            card.reveal();
            return true;
        }
    }

    public void cleanHand() {
        for (int i = hand.size()-1; i >= 0; i--) {
            Row row = hand.get(i);
            if (row.canBeCleared()) {
                hand.remove(i);
            }
        }
    }

    public int getRowCount() {
        return hand.size();
    }

    public void revealAll() {
        for (Row row : hand) {
            for (Card card : row) {
                card.reveal();
            }
        }
    }
}
